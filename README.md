# Pablo Portugués

### Setup new icons

- Go to [https://icomoon.io/app/#/select/](https://icomoon.io/app/#/select/)
- Select all pretended icons and hit `Generate Font`
- Rename as needed
- Hit `Download`
- Copy the content of the `fonts` folder into your project
- Copy the `style.css` content into your main `.css` file
- Adjust the path of the files if needed