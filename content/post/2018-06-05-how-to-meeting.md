---
author: "Pablo Portugués"
date: 2018-06-05
linktitle: How to meeting
menu:
  main:
    parent: general
next: 
prev: 
title: How to meeting
description: We all have meetings and we have often question why do we have them. This will maybe help you ease that pain
weight: 10
keywords: [
  "time",
  "meetings",
  "communication"
]
tags: [
    "general"
]
---


{{< figure src="/post-content/2018-06-05/meetings-to-know-why-we-have-meetings.png" class="post-image" >}}

We all have meetings, some more than others, but there's an usual common feeling that people share, which is that excessive and unclear/unnecessary meetings lead to frustration and a lack of productivity. They can break, or even prevent you from starting, your thought process, because you know you're gonna have a meeting soon.

Given that a meeting-less scenario could never be a reality (you can try though), myself with the aid of some team members, decided to create a guide to prevent (or at least mitigate) the biggest impacts.

1. [Scheduling a meeting](#scheduling-a-meeting)
1. [Attending a meeting](#attending-a-meeting)

**NOTE:** These are guidelines and like life, shouldn't be taken to the letter. Experiment and iterate until find what best suits you.

---

### Scheduling a meeting

When you're scheduling a meeting you should always have the following into consideration:

#### Before

1. Do I need a meeting?

Is it something that you could clarify or decide via Slack/email with a group of people? Then probably you don't need a meeting. Is it something that will trigger heavy discussions and is very intensive? A meeting would be a good call for this one.

1. What is the objective?

A meeting without a specific objective(s) turns into a conversation in which you usually don't get anywhere. Therefore meeting should have an agenda, a clear goal of it's purpose. Ask yourself, what do I want to get out of this? What needs to be decided?

1. Who should attend?

Have you ever been in a meeting where you thought: why am I here? is this real life? Put yourself on the attendees shoes and think if it makes sense for you to be there. Also the greater the number of people in the meeting, the harder it is to conduct it.

1. How much time should I allocate?

As less as possible. This doesn't mean you can't do 1 hour meetings, it means that you should aim to be as efficient as possible so it takes the shortest amount of time. You can do 10min, 15min, even 7min, it depends on what the meeting is about. Remember, if you have an 1 hour meeting with 8 people, it represents 8 hours of which people weren't doing something else, plus the individual focus time to switch back to what you were doing.

#### During

1. Who should lead the meeting?

Usually it tends to be the person who scheduled it, however it may not be always the case. In that scenario, prior to the meeting, a moderator should be appointed to lead the meeting towards achieving it's objective(s).

1. Some time has passed not all people have yet joined, do we continue to wait?

When someone's late it's not just a 5/10 minute loss, it's that times everyone attending, minus the time you won't have for the actual meeting because it was spent waiting. So the recommendation is: If the **meeting is < 30min** wait up to **3 minutes**, if **more than > 30mi** wait up to **5 minutes**. After that if you have the necessary people to conduct it, then start, if not then drop it and reschedule if necessary.

1. How should I begin?

It's up to the moderator, but we recommend doing a quick recap of what the meeting is about, what we know, what we're trying to do and kick off discussion on how we can do it.

1. What if everyone decides to bring their laptops and nobody is paying attention and just nodding their heads?

Unless it's a remote-only meeting or there is a specific reason for people bringing their laptop (read: for the purpose of the meeting), nobody should be typing away or looking at their screen. If you've done the prior work of setting it all up and are putting the effort to be as efficient as possible, it's a sign of disrespect to not be paying attention. The same applies to the use of cellphones. However, usually when that happens it means that person hasn't been participative for a while, so it might be that the meeting is taking to long or they weren't necessary in the first place.

1. We're now discussing the meaning of life and people seem committed to it, but it has nothing to do with it's objective, how do we circle back around?

Sometimes, even if you have a very specific focus, things can get off-track. Main thing here is to guarantee the conversation gets brought back on track. Anyone can do this, but the moderator should chip in and readjust the discussion so it's aligned with the meeting's objectives.
<br/>
This is not to say every off-topic is unimportant, because they might be, but in that case it should be part of a different discussion with that as its objective.

1. I'm usually the moderator but I struggle to keep record of what's important without affecting the meeting pace, what can I do?

The moderator isn't necessarily the one who should keep record of what's important, in fact, there is usually a scribe for that. In this case you should appoint a scribe that will do all the recording of what's important.

#### After

1. What to do after a meeting is done?

Now it's time to share. Create a draft (or something that works as a summary of the meeting) and share it with all who attended, plus any other person to whom the meeting purpose is relevant. If you had a scribe appointed, that should be the person to share this.

##### After all this preparation, how was the meeting?

This is something you could try to answer yourself, or go the extra mile and ask the participants about it. Do one-on-one quick conversations about the ups and downs or just send out a survey. Main point here is to gather useful feedback and get better at it.

---

### Attending a meeting

When you're attending a meeting you should always have the following into consideration:

#### Before

1. What is the objective?

Make sure it's clear to you what the objective(s) is(are) prior to attending the meeting. Ask the organizer, in case you have doubts so they can be clarified. This is very important so that you are on the same page as everyone else and can prepare beforehand.

1. Why have I even been invited?

That's a good question, maybe you shouldn't even be there and should reject it, but maybe you're missing important information about it's purpose and the role you play in it. Reach out to whoever's organizing so that you can understand why.

1. An 1 hour meeting to discuss labels, really?

Indeed, we'd probably have time to discuss the weather, or would we? It might happen that the allocated time is overkill, but you'll only know that when you talk with the organizer. In the end you might end up agreeing to it or it might just get chopped in half. Ask away!

1. I won't be able to make it, they'll be fine without me, right?

Maybe, maybe not, depends on how important is your participation. Regardless you should always notify the other attendees (or at least the organizer) that you won't be coming. Either they will proceed and know that you aren't coming or the meeting will be rescheduled if you really need to be present. If case is the latter, then propose time slots in which you are available so it's easier to reschedule.

#### During

1. Can I take my laptop? It's just for quick replies if anybody needs me.

You're going to a meeting that someone put in the effort to prepare so it's as short and as productive as possible, if you bring your laptop when it's not clearly stated as needed, then you'd be disrespecting and harming the flow of the meeting as you'd not be fully attentive and focused on what's being discussed. Remember, for meetings to be quick and focused it just depends on whoever's in them, yourself included.

1. Now they're actually talking about the weather, am I just wasting my time?

If you feel like things are getting off-track, step in and readjust the conversation direction. Although this is usually done by the moderator, it's not exclusive, so do help out.

#### After

1. What to do after a meeting is done?

If the output of the meeting had any action items assigned to you, then follow up on those, if not, grab some coffee/look at cat pictures on the internet.

##### After all this, how was the meeting?

Be proactive and share, constructively, what you liked and what you think could be improved. Remember that a lot of work goes into creating and executing a meeting, so it's always good to appreciate it, especially when it was useful.

---

We actually did a meeting to discuss how we should do meetings!