---
author: "Pablo Portugués"
date: 2018-05-29
linktitle: The four types of remote work
menu:
  main:
    parent: general
next: 
prev: 
title: The four types of remote work
description: An overview about pros and cons of different types of remote work
weight: 10
keywords: [
  "work",
  "remote",
  "communication"
]
tags: [
    "remote"
]
---

{{< figure src="/post-content/2018-05-29/remote-working-like-a-boss.jpg" class="post-image" >}}

Remote work interests me very much, and I've been very keen on getting to know more, especially because of communication. You need to (almost) relearn the way you communicate since the the person is not there, like physically in front of you, but on the other hand it unlocks a whole new set of possibilities and also helps a lot with existing communication issues that even non-remote companies have.

With that said, from my experience, I put remote work into four categories:

1. [No remote](#no-remote)
1. [Remote friendly](#remote-friendly)
1. [Remote first](#remote-first)
1. [Remote only](#remote-only)

You're probably wondering why _No remote_ is even there. Well, it is a reality for the higher majority of workers and if we aim for a proper comparison, it should be here as well.


### No remote

No remote is the reality for most of us. For you to work you need be at a specific place, everyday, for a determined amount of time. People expect to find you (and everybody else) there. If I want to talk to you I can drop by your seat because I know you'll be present.

#### Pros

1. You have the chance to connect with everyone in person
1. Everybody is on the same timezone
1. It's usually easier to do meetings because everyone is right there and on that meeting
1. Free snacks and coffee (if that's something your company provides)
1. Communication is easier because you're just talking directly to people

#### Cons

1. You commute. E-v-e-r-y-d-a-y.
1. You're limited to the office conditions you're given (probably an open space with lots of people and noise)
1. You have fixed working hours (it could not be the case)
1. Information usually gets lost in water cooler conversations because it's not written down
1. People you meet are usually from around the area. If you're hiring you're limited to whom is already there or wants to relocate


### Remote friendly

This is when most of the company works in office, but it's alright if you want to work remotely. It helps tremendously in situations like when your kid is sick, or when you have doctor's appointments and you live far from the office, or just when you're so tired that the 3 hours a day you spend commuting will be better spent resting.

#### Pros

1. You can do a better balance of your personal and work life, i.e., kid's sick, you got appointments, you could just work from where it's best for you
1. No commute for the days you're remote
1. You still go to the office so you can connect with everyone in person

#### Cons

1. Sometimes you're treated like a second-class citizen (work wise that is)
1. Meetings can be very hard if people are not aware. You might end up connected to one laptop in a room with several people, talking at the same time, making it very hard for you to hear and to participate. They may also point the webcam to the wall for you to see what's being drawn in the board or the post its
1. You miss out on important information because it happened on a discussion with people in the office and nobody remembered you weren't there, or maybe because it was very quick and setting up the whole thing was time consuming and not worth it. People also didn't write it down, because it all happened so naturally
1. You choose your remote days carefully as to not collide with important meetings that people don't know how to do remote, like groomings, retros and that discussion about how cats rule the internet


### Remote first

When you have offices and people are allowed to manage their time and workplace as they see fit, you have the room to be remote first. If everybody's in the office, you can just do it all offline, but if a single person is remote, then all proceedings are remote first. This means having proper support for remote meetings (audio and video) if you do them mixed (people in meeting room, people remote), or just do them remote by default, i.e., each person is connected to an online meeting. All key decisions have to reach everyone, they need to be accessible, so writing them down is key!

#### Pros

1. You pick where you want to work: office, home or outer space (just remember to take your hotspot).
1. No commute if you're not going to the office
1. There's an office, so you could still go there and connect in person
1. You feel included and not left out of important information or conversations just because you were not in the office

#### Cons

1. If you remote from a different country (far away place) you lose direct interaction with other people that get a chance to go into the office
1. In mixed meetings you might still feel a little bit left out, because it's always different from being present
1. Creating relationships with your teammates is harder. You're not there to go and grab a beer


### Remote only

There are **no** offices (just a mailbox, perhaps, with a sticker). Everybody you work with is in the same situation you are, but you choose where you want to be, as does everybody else. (Canada, here I come!)

#### Pros

1. No (or shorter) commute - whether at home, co-working, coffee place or bunny island, you chose where you want to work
1. Because everybody is in the same scenario, there is more awareness and concern towards information and interactions, so you don't feel left out
1. You'll probably work with and meet people from many different countries and locations
1. You can hire from anywhere in the world 🌍 (local financial laws will probably apply)
1. Usually, once a year, you go to a cool new place for a get together with those nerds you talk to every day, so that you can nerd awkwardly with each other

#### Cons

1. You might miss interacting with people (yes, that can happen)
1. Timezones can be a bitch
1. You need to learn new ways of communicating that are not synchronous (if you've never worked this way before)

---

I remote. Do you remote?