---
author: "Pablo Portugués"
date: 2017-06-27
linktitle: It does matter where you put it
menu:
  main:
    parent: general
next: 
prev: 
title: It does matter where you put it
description: The importance of where to publish what you want to share with the world
weight: 10
keywords: [
  "facebook",
  "content",
  "sharing",
  "visibility",
  "privacy",
  "user experience",
  "availability"
]
tags: [
    "general",
    "sharing",
    "content"
]
---

Whenever you want to write something and share it with the open world, where do you put that content? Why did you decide to put it there? How many people can it reach?

I've built this blog because I wanted to write, and I hosted it on GitLab pages because I want to [control the content]({{< ref "2017-06-13-it-actually-works.md" >}}), so it makes sense for me that anything people want to share with the world, should be reachable by everyone. Facebook doesn't do that. I'll try to explain you the importance of this, so maybe next time you want to share something with the world, you're actually doing what you intended to.

If you have a Facebook account and decide to write something for everyone to read, you need to have in mind certain things, like the post's privacy settings. It could easily be accessible to everyone that has an account, but not to everybody who hasn't (yes, there are people who don't have a Facebook account, and yes, they're normal people like you). Even if you do set it as public, for someone who's not logged in, this is the experience they get:

![facebook experience](/post-content/2017-06-27/facebook.gif)

Also, you have no control whatsoever about formatting. Wanted to add some **bold** or _italic_ text, a [custom link](https://daringfireball.net/projects/markdown/basics) perhaps? Some `code` even? Well, you can forget it, because they know what's best for their platform (yes, Facebook is a private company that will always look after their own interests, never yours, don't forget that!).

One other thing to have in consideration is that every post you write isn't saved by The Internet Archive because [Facebook doesn't allow it] (https://web.archive.org/web/*/https://www.facebook.com/marc.haynes.7583/posts/793204930854561?match=bWFyYyBoYXluZXMscm9nZXIgbW9vcmU%3D), **nor it is indexed by search engines**, meaning Google or other search engines won't return your post, unless the link to it lives elsewhere, and that elsewhere is indexed (like a blog for example). The only way I could have access to it is by searching within Facebook, and for that you need an account.

Now, you may even think: "Well, why don't you create an account so you never have to bother again?" To which my answer is: _There are a [lot](http://www.nybooks.com/articles/2016/12/22/they-have-right-now-another-you/) of [reasons](https://www.technologyreview.com/s/604082/we-need-more-alternatives-to-facebook/) why you [wouldn't](https://veekaybee.github.io/facebook-is-collecting-this/) want to have a Facebook [account](https://www.washingtonpost.com/news/the-intersect/wp/2016/08/19/98-personal-data-points-that-facebook-uses-to-target-ads-to-you/), and just stating that you don't want one should be enough._

Even so, if you still don't care about that, there are additional problems. You don't control Facebook, so you don't know for how long that content will live there. It is a known fact that Facebook is in good health and will probably hang around a lot longer, but if they decide to change their privacy policy, delete your content or block your account, you have no control whatsoever.

My main reason for all this rambling is to try and bring some awareness. The web is supposed to be open, Facebook is the exact opposite of that. Do you want to share **YOUR** content with the world? Do you care what happens to that content long term? Then place it somewhere you control and that's openly available for everyone with an internet connection. Link whatever you want (Facebook, Twitter, Pinterest, etc) there, not the other way around!

So how could you achieve this? Well, you have several ways of doing it. GitHub pages offers a very simple way of accomplishing this with [Jekyll] (https://pages.github.com/). You just need to learn markdown in order to write content and with GitHub desktop app, you don't even need to open the terminal to update **your very own blog**.

Ultimately, if you really don't want the trouble of going with a static pages blog, you can choose something like [Blogger](https://www.blogger.com/), [WordPress](https://wordpress.org/) or even [Medium](https://medium.com), which has gained quite some traction lately.

By doing this you'll provide your content to the world wide web for anyone to access it anywhere! It will be searchable by your search engine of choice, it will be stored on the Internet Archive and you'll be able to provide the same user experience for anyone that may wish to consume it.

---
Some [bedtime reading](http://www.nybooks.com/articles/2016/12/22/they-have-right-now-another-you/).